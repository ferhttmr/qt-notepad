#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setCentralWidget(ui->my_Text_Area);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionNew_triggered()
{
    current_file.clear();
    ui->my_Text_Area->setText(QString());
}

void MainWindow::on_actionOpen_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this, "Open the file");
    current_file = file_name;
    QFile file(file_name);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot open file : " + file.errorString());
        return;
    }

    setWindowTitle(file_name);

    QTextStream in(&file);
    QString text = in.readAll();
    ui->my_Text_Area->setText(text);
    file.close();

}

void MainWindow::on_actionSave_as_triggered()
{
    QString file_name = QFileDialog::getSaveFileName(this, "Save as");
    current_file = file_name;
    QFile file(file_name);
    if(!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file : " + file.errorString());
        return;
    }
    setWindowTitle(file_name);

    QTextStream out(&file);
    QString text = ui->my_Text_Area->toPlainText();

    out<<text;
    file.close();

}

void MainWindow::on_actionPrint_triggered()
{
    QPrinter printer;
    printer.setPrinterName("My printer");
    QPrintDialog p_dialog(&printer, this);

    if(p_dialog.exec() == QDialog::Rejected)
    {
        QMessageBox::warning(this, "Warning", "Cannot acess print");
        return;
    }

    ui->my_Text_Area->print(&printer);
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionCopy_triggered()
{
    ui->my_Text_Area->copy();
}

void MainWindow::on_actionPaste_triggered()
{
    ui->my_Text_Area->paste();
}

void MainWindow::on_actionCut_triggered()
{
    ui->my_Text_Area->cut();
}

void MainWindow::on_actionUndo_triggered()
{
    ui->my_Text_Area->undo();
}

void MainWindow::on_actionRedo_triggered()
{
    ui->my_Text_Area->redo();
}
